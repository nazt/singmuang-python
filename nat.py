#!/usr/bin/python
import cv
import sys

if __name__ == '__main__':
    if len(sys.argv) != 2:                                         ## Check for error in usage syntax

        print "Usage : python faces.py <image_file>"

    else:
		imcolor = cv.LoadImage(sys.argv[1]) # input image
		replaceImage = cv.LoadImage("cat.png") # input image
		# loading the classifiers
		haarFace = cv.Load('haarcascade_frontalface_alt.xml')
		haarEyes = cv.Load('haarcascade_eye_tree_eyeglasses.xml')

		# running the classifiers
		storage = cv.CreateMemStorage()
		detectedFace = cv.HaarDetectObjects(imcolor, haarFace, storage)
		detectedEyes = cv.HaarDetectObjects(imcolor, haarEyes, storage)

		# draw a green rectangle where the face is detected
		if detectedFace:
		 for face in detectedFace:
		  cv.Rectangle(imcolor,(face[0][0],face[0][1]),
		               (face[0][0]+face[0][2],face[0][1]+face[0][3]),
		               cv.RGB(155, 255, 25),2)

		# draw a purple rectangle where the eye is detected
		if detectedEyes:
		 for face in detectedEyes:
		  cv.Rectangle(imcolor,(face[0][0],face[0][1]),
		               (face[0][0]+face[0][2],face[0][1]+face[0][3]),
		               cv.RGB(155, 55, 200),2)

		mask = cv.CreateImage(detectedFace[0][0][2], detectedFace[0][0][3])

		# cv.Resize(replaceImage, mask)
		print(detectedFace[0][0][2])
		print(replaceImage)
		print(detectedFace)
		# imcolor[0:100, 0:100] = replaceImage
		cv.NamedWindow('Face Detection', cv.CV_WINDOW_AUTOSIZE)
		cv.ShowImage('Face Detection', imcolor) 
		cv.WaitKey()
